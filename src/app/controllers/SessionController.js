import jwt from 'jsonwebtoken';
import * as Yup from 'yup';

import User from '../models/User';

import authConfig from '../../config/auth';

class SessionController {
  async create(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string().required(),
      password: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      res.status(400).json({ error: 'Resquest with problems' });
    }

    const user = await User.findOne({ where: { email: req.body.email } });

    if (!user) {
      res.status(401).json({ error: 'User is not valid' });
    }

    if (!(await user.checkPassword)) {
      res.status(401).json({ error: 'Password does not match' });
    }

    const token = jwt.sign({ id: user.id }, authConfig.secret, {
      expiresIn: authConfig.expiredIn,
    });

    res.json({
      user,
      token,
    });
  }
}

export default new SessionController();
