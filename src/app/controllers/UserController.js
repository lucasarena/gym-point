import * as Yup from 'yup';

import User from '../models/User';

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().required(),
      password: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      res.status(400).json({
        error: 'Fields sended are not correct',
      });
    }

    const user = await User.create(req.body);

    res.json(user);
  }
}

export default new UserController();
